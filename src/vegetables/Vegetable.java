/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetables;

/**
 *
 * @author dannylowpass
 */
abstract class Vegetable {
    String colour;
    double size;
   
    
    public Vegetable(){
        
    }
    
    public Vegetable (String colour, double size){
        this.colour = colour;
        this.size = size;
    }
    
    String getColour(){
        return colour;
    }
    
    double getSize(){
        return size;
    }
    
    abstract boolean isRipe(String colour, double size);
} 

