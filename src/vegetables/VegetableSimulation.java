/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetables;

/**
 *
 * @author dannylowpass
 */
public class VegetableSimulation {
    
    public static void main(String[] args){
        
        VegetableFactory factory = VegetableFactory.getInstance();

        Beet unripeBeet = new Beet("Green", 1.0);
        Carrot unripeCarrot = new Carrot("Yellow", 1.0);
        
        Beet ripeBeet = new Beet("Red", 1.0);
        Carrot ripeCarrot = new Carrot("Orange", 2.0);
        
        System.out.println("Carrot is ripe?", ripeCarrot.isRipe());
        System.out.println("Beet is ripe?", ripeBeet.isRipe());
        System.out.println("Carrot is ripe?", unripeCarrot.isRipe());
        System.out.println("Beet is ripe?", unripeBeet.isRipe());
    }
}
